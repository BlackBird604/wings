<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::post('/', 'PagesController@index');
Route::get('/contact', 'PagesController@contact');

Route::resource('colors', 'ColorController');
Route::resource('sizes', 'SizeController');
Route::resource('productTypes', 'ProductTypeController');
Route::resource('products', 'ProductController');
Route::get('/search', 'ProductController@search');
Route::get('/cart', 'CartController@index');
Route::post('/cart/addProduct/{id}', 'CartController@addProduct');
Route::delete('/cart/destroy/{id}', 'CartController@destroy');
Route::get('/cart/clear', 'CartController@clear');
Route::get('/cart/buy', 'CartController@buy');


Auth::routes();
Route::get('/dashboard', 'DashboardController@index');
Route::get('/administration', 'AdministrationController@index');
Route::get('/administration/products', 'AdministrationController@productList');
Route::get('/administration/colors', 'AdministrationController@colorList');
Route::get('/administration/sizes', 'AdministrationController@sizeList');
Route::get('/administration/productTypes', 'AdministrationController@productTypeList');
