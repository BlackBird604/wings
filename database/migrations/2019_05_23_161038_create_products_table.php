<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
            $table->integer('amount');
            $table->decimal('price', 6, 2);
            $table->string('image')->default('defaultThumbnail.png');
            $table->text('description')->nullable();
            $table->integer('gender');

            $table->bigInteger('color_id')->unsigned();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');

            $table->bigInteger('productType_id')->unsigned();
            $table->foreign('productType_id')->references('id')->on('product_types')->onDelete('cascade');

            $table->bigInteger('size_id')->unsigned();
            $table->foreign('size_id')->references('id')->on('sizes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign(['color_id']);
            $table->dropForeign(['productType_id']);
            $table->dropForeign(['size_id']);
        });

        Schema::dropIfExists('products');
    }
}
