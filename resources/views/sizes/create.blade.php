@extends('layouts/app')

@section('content')
  <h1>Dodawanie rozmiaru</h1><br>
  {!! Form::open(['action' => 'SizeController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    {{csrf_field()}}

    <div class="form-group">
      {!! Form::label('shortName', 'Skrót', ['class' => 'control-label']) !!}
      {!! Form::text('shortName', old('shortName'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}<br>

      {{Form::submit('Dodaj', ['class'=>'btn btn-primary'])}}
    </div>
  {!! Form::close() !!}

@endsection
