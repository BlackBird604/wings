@extends('layouts/app')

@section('content')
  <h1>Edytowanie rozmiaru</h1><br>
  {!! Form::open(['action' => ['SizeController@update', $size->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    {{csrf_field()}}

    <div class="form-group">
      {!! Form::label('shortName', 'Skrót', ['class' => 'control-label']) !!}
      {!! Form::text('shortName', old('shortName', $size->shortName), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}<br>

      {{Form::submit('Zapisz', ['class'=>'btn btn-primary'])}}
    </div>
    {{Form::hidden('_method','PUT')}}
  {!! Form::close() !!}

@endsection
