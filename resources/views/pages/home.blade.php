@extends('layouts/app')

@section('content')
<div class="container text-center">
  <h1 class="jumbotron-heading">Witamy na stronie Wings!</h1>
  <p class="lead text-muted">Projektem jest aplikacja webowa wykonana przy użyciu frameworka Laravel.<br>
  Portal Wings stworzony jest z myślą o miłośnikach ptaków, którzy chcieliby rozszerzyć swoją wiedzę,<br>
  by móc efektywniej pomagać swoim skrzydlatym przyjaciołom.<br>
  Zapraszamy do korzystania!</p>
</div>

@endsection
