@extends('layouts/app')

@section('styles')
  <link href="{{ asset('css/cart.css') }}" rel="stylesheet">
@endsection

@section('content')
  <div class="card">
    @include('inc.cart.header')
    <ul class="list-group list-group-flush">
      @php
        $cartTotal=0;
      @endphp

      @foreach($cartEntries as $cartEntry)
        @php
          $product = $products->firstWhere('id', $cartEntry->product_id);
          $size = $sizes->firstWhere('id', $product->size_id);
          $adjustedAmount = $cartEntry->amount > $product->amount ? $product->amount : $cartEntry->amount;
          $entryPrice = $product->price * $adjustedAmount;
          $cartTotal = $cartTotal + $entryPrice;

        @endphp
        @include('inc.cart.entry')
      @endforeach
    </ul>

    @include('inc.cart.footer')
  </div>
  @if($cartEntries->count())
    @include('inc.cart.summary')
  @endif
@endsection
