@extends('layouts/app')

@section('content')
  <h1>Edytowanie koloru</h1><br>
  {!! Form::open(['action' => ['ColorController@update', $color->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    {{csrf_field()}}

    <div class="form-group">
      {!! Form::label('name', 'Nazwa', ['class' => 'control-label']) !!}
      {!! Form::text('name', old('name', $color->name), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}<br>

      {!! Form::label('value', 'Kolor', ['class' => 'control-label']) !!}
      {!! Form::text('value', old('value', $color->value), ['class' => 'form-control', 'id' => 'color-value', 'placeholder' => '']) !!}<br>

      {{Form::submit('Zapisz', ['class'=>'btn btn-primary'])}}
    </div>
    {{Form::hidden('_method','PUT')}}
  {!! Form::close() !!}

@endsection


@section('scripts')
    <script>
        $('#color-value').colorpicker({});
    </script>
@endsection
