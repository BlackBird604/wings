@extends('layouts/app')

@section('content')
  <h1>Dodawanie koloru</h1><br>
  {!! Form::open(['action' => 'ColorController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    {{csrf_field()}}

    <div class="form-group">
      {!! Form::label('name', 'Nazwa', ['class' => 'control-label']) !!}
      {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}<br>

      {!! Form::label('value', 'Kolor', ['class' => 'control-label']) !!}
      {!! Form::text('value', old('value'), ['class' => 'form-control', 'id' => 'color-value', 'placeholder' => '']) !!}<br>

      {{Form::submit('Dodaj', ['class'=>'btn btn-primary'])}}
    </div>
  {!! Form::close() !!}

@endsection


@section('scripts')
    <script>
        $('#color-value').colorpicker({});
    </script>
@endsection
