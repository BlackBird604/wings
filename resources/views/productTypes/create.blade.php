@extends('layouts/app')

@section('content')
  <h1>Dodawanie rodzaju produktu</h1><br>
  {!! Form::open(['action' => 'ProductTypeController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    {{csrf_field()}}

    <div class="form-group">
      {!! Form::label('name', 'Nazwa', ['class' => 'control-label']) !!}
      {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}<br>

      {{Form::submit('Dodaj', ['class'=>'btn btn-primary'])}}
    </div>
  {!! Form::close() !!}

@endsection
