@extends('layouts/app')

@section('content')
  <h1>Edytowanie rodzaju produktu</h1><br>
  {!! Form::open(['action' => ['ProductTypeController@update', $productType->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    {{csrf_field()}}

    <div class="form-group">
      {!! Form::label('name', 'Nazwa', ['class' => 'control-label']) !!}
      {!! Form::text('name', old('name', $productType->name), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}<br>

      {{Form::submit('Zapisz', ['class'=>'btn btn-primary'])}}
    </div>
    {{Form::hidden('_method','PUT')}}
  {!! Form::close() !!}

@endsection
