<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style="width: 100%; width: 100vw;">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{config('app.name', 'No Name')}}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/global.css') }}" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

        <!-- jQuery + Bootstrap + Ajax -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/js/bootstrap-colorpicker.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/HideEmptyURL.js') }}"></script>

        @yield('styles')
    </head>

    <body class="background-img">
      @include('inc.navbar')
      <div class="container" style="padding-top:20px;">
        @include('inc.messages')
        @yield('content')

      </div>
      @include('inc.footer')

      <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
      <script> CKEDITOR.replace( 'article-ckeditor' ); </script>

      @yield('scripts')
    </body>
</html>
