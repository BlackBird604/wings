@extends('layouts/app')

@section('styles')
  <link href="{{ asset('css/administration.css') }}" rel="stylesheet">
  <link href="{{ asset('css/global.css') }}" rel="stylesheet">
@endsection

@section('content')
  <div class="container text-center">
    <div class="row w-100 text-center">
      <div class="card w-100 h-100 text-center m-0 p-0">
        <div class="card-header bg-dark text-light">
          <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
              <a class="nav-link {{$tabNumber == 0 ? 'active' : ''}}" href="/administration/products">Produkty</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{$tabNumber == 1 ? 'active' : ''}}" href="/administration/colors">Kolory</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{$tabNumber == 2 ? 'active' : ''}}" href="/administration/sizes">Rozmiary</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{$tabNumber == 3 ? 'active' : ''}}" href="/administration/productTypes">Rodzaje produktów</a>
            </li>
            <li class="ml-auto nav-item">
              @switch($tabNumber)
                @case(0)
                  <a class="nav-link text-warning" href="/products/create">Nowy produkt</a>
                @break

                @case(1)
                  <a class="nav-link text-warning" href="/colors/create">Nowy kolor</a>
                @break

                @case(2)
                  <a class="nav-link text-warning" href="/sizes/create">Nowy rozmiar</a>
                @break

                @case(3)
                  <a class="nav-link text-warning" href="/productTypes/create">Nowy rodzaj produktu</a>
                @break
              @endswitch
            </li>
          </ul>
        </div>

        <div class="card-body bg-light pt-2">
          <div class="row w-100">
            @switch($tabNumber)
              @case(0)
                @include('inc.administration.productList')
              @break

              @case(1)
                @include('inc.administration.colorList')
              @break

              @case(2)
                @include('inc.administration.sizeList')
              @break

              @case(3)
                @include('inc.administration.productTypeList')
              @break
            @endswitch
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
