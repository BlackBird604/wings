@extends('layouts/app')

@section('styles')
  <link href="{{ asset('css/showProduct.css') }}" rel="stylesheet">
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/inputSpinner.js') }}"></script>
  <script>
      $("input[type='number']").inputSpinner()
  </script>
@endsection

@section('content')
  <div class="container text-center">
    <div class="row w-100 text-center">
      <div class="col-lg-4 pt-2 text-dark border-right" style="min-height:600px">
        <img src={{asset("storage/images/$product->image")}} class="product-image mb-2">
        <h1 style="font-variant:small-caps">{{$product->name}}</h1>
        <h2>{{$product->price}}zł</h2>

        @auth
        {!! Form::open(['action' => ['CartController@addProduct', $product->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
          {{csrf_field()}}
          <div class="row d-flex justify-content-center mb-2">
            <div style="width:120px">
              <input type="number" name="amount" value="1" min="1" max="{{$product->amount}}" step="1">
            </div>
          </div>
          {{Form::submit('DODAJ DO KOSZYKA', ['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
        @else
          <h6><i>Zaloguj się, aby dodać do koszyka</i></h6>
        @endauth
      </div>


      <div class="col-lg-8 pl-5 pt-3 h-100">
        <div class="row d-flex align-items-baseline border-top">
          <h1 class="mr-2" style="font-variant:small-caps">
            {{$product->name}} -
          </h1>
          <h2>
            <small>
              {{$productType->name}}
            </small>
          </h2>
        </div>

        <div class="row d-flex align-items-baseline border-bottom mb-2 pb-0">
          <h2>
            <b>{{$product->price}} zł</b>
            <small class="text-muted">
              w tym VAT
            </small>
          </h2>
        </div>

        <div class="row d-flex align-items-center mb-2">
          <h4 class="m-0 mr-1 mb">
            Kolor: <b>{{$color->name}}</b>
          </h4>
          <div class="color-box" style="background-color:{{$color->value}}"></div>
        </div>



        <div class="row d-flex align-items-baseline">
          <h4 class="pr-2">
            Rozmiar: <b>{{$size->shortName}}</b>
          </h4>
        </div>

        <div class="row d-flex align-items-baseline">
          <h4 class="pr-2">
            Płeć:
          </h4>
            @switch($product->gender)
              @case(1)
                <img class="gender-icon" src={{asset("storage/icons/male.png")}}>
              @break
              @case(2)
                <img class="gender-icon" src={{asset("storage/icons/female.png")}}>
              @break
              @case(3)
                <img class="gender-icon" src={{asset("storage/icons/male.png")}}>
                <img class="gender-icon" src={{asset("storage/icons/female.png")}}>
              @break
            @endswitch
        </div>

        <div class="row d-flex align-items-center pb-3 border-bottom">
          @if($product->amount < 5)
            @php
              $text_color = "#d10000"
            @endphp
          @elseif($product->amount <= 20)
            @php
              $text_color = "#ff7b00"
            @endphp
          @else
            @php
              $text_color = "#35a000"
            @endphp
          @endif

          <h5 class="m-0 mr-1">
            Dostępność:
          </h5>
          <div class="amount-box p-0 px-1" style="border-radius:5px; border-width:2px; border-style:solid; border-color:{{$text_color}};">
            <h5 class="m-0 p-0" style="color:{{$text_color}}">
              <b>{{$product->amount}}</b>
            </h5>
          </div>
        </div>

        <div class="row d-flex align-items-center mb-1 mt-1">
          <h4 class="m-0 mr-1 mt-1">
            Opis:
          </h4>
        </div>
        <div class="row pl-4 d-flex align-items-baseline">
            {!!$product->description!!}
        </div>

        <div class="row d-flex align-items-center border-top border-bottom">
          <img class="delivery-icon" src={{asset("storage/icons/delivery.png")}}>
          <h6 class="ml-2 mt-2">
            Przesyłka standardowa gratis<br>
            w ciągu 2-5 dni roboczych
          </h6>
        </div>

      </div>
    </div>
  </div>
@endsection
