@extends('layouts/app')

@section('styles')
  <link href="{{ asset('css/createProduct.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script type="text/javascript">
      var thumbnail_path = <?php echo json_encode(asset("/storage/images/defaultThumbnail.png")); ?>;
    </script>

    <script type="text/javascript" src="{{ asset('js/productThumbnailUpload.js') }}"></script>
@endsection

@section('content')
  <h1>Dodawanie produktu</h1><br>
  {!! Form::open(['action' => 'ProductController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    {{csrf_field()}}

    <div class="form-group">
      {{Form::label('name', 'NAZWA:')}}
      {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => "Nazwa produktu"])}}<br>

      <div class="row">
        <div class="col-md-4">
          {{Form::label('productType', 'RODZAJ:')}}
          <select name="productType" class="form-control browser-default custom-select">
            @foreach($productTypes as $productType)
              <option {{old('productType') == $productType->id ? 'selected' : ''}} value="{{ $productType->id }}">{{ $productType->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-4">
          {{Form::label('size', 'ROZMIAR:')}}
          <select name="size" class="form-control browser-default custom-select">
            @foreach($sizes as $size)
              <option {{old('size') == $size->id ? 'selected' : ''}} value="{{ $size->id }}">{{ $size->shortName }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-4">
          {{Form::label('gender', 'PŁEĆ:')}} <br>
          @include('inc.genderSelector')
        </div>
      </div>
      <br>


      {{Form::label('color', 'KOLOR:')}}
      @include('inc.colorSelector')

      <div class="row">
        <div class="col-md-6">
          {{Form::label('amount', 'ILOŚĆ:')}}
          {{Form::number('amount', old('amount', 1), ['class' => 'form-control'])}}
        </div>
        <div class="col-md-6">
          {{Form::label('price', 'CENA:')}}
          <div class="input-group mb-3">
            <input type="text" name="price" class="form-control" placeholder="Cena" value="{{old('price')}}">
            <div class="input-group-append">
              <span class="input-group-text">zł</span>
            </div>
          </div>
        </div>
      </div>
      <br>

      {{Form::label('thumbnail', 'ZDJĘCIE:')}}
      <div class="mb-2" id='product-thumbnail'>
        <div class="hvr-profile-img"><input type="file" class="upload" id="thumbnail-input" name="thumbnail" accept="image/png, image/jpeg">></div>
        <i class="fa"><img src="{{ asset('storage/icons/camera.png') }}" width="25px" height="25px"></i>
      </div>
      <br>

      {{Form::label('description', 'OPIS:')}}
      {{Form::textarea('description', old('description'), ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => "Opis produktu..."])}}<br>

      {{Form::submit('Dodaj', ['class'=>'btn btn-primary'])}}
    </div>
  {!! Form::close() !!}

@endsection
