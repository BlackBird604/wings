@extends('layouts/app')

@section('styles')
  <link href="{{ asset('css/offer.css') }}" rel="stylesheet">
@endsection

@section('content')
@include('inc.filter')
  <div class="card m-2 pt-2">
      <div class="card-body pt-0">
      <div class="row pt-0">
        @foreach($products as $product)
          @include('inc.productCard')
        @endforeach
      </div>
    </div>
  </div>

  @if (!is_null($products))
      {{$products->appends(request()->query())->links()}}
    @endif
@endsection
