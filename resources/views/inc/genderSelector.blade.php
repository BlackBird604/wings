<div class="card h-50 justify-content-center">
  <div class="row justify-content-center">
    <div class="custom-control custom-radio custom-control-inline col-3 justify-content-center">
      @if(empty($product))
        <input type="radio" class="custom-control-input" id="GenderM" name="gender" value="1" {{ old('gender') == "1" ? 'checked' : '' }}>
      @else
        <input type="radio" class="custom-control-input" id="GenderM" name="gender" value="1" {{ old('gender', $product->gender) == "1" ? 'checked' : '' }}>
      @endif
      <label class="custom-control-label" for="GenderM">M</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline col-3 justify-content-center">
      @if(empty($product))
        <input type="radio" class="custom-control-input" id="GenderF" name="gender" value="2" {{ old('gender') == "2" ? 'checked' : '' }}>
      @else
        <input type="radio" class="custom-control-input" id="GenderF" name="gender" value="2" {{ old('gender', $product->gender) == "2" ? 'checked' : '' }}>
      @endif
      <label class="custom-control-label" for="GenderF">K</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline col-3 justify-content-center">
      @if(empty($product))
        <input type="radio" class="custom-control-input" id="GenderMF" name="gender" value="3" {{ old('gender') == "3" ? 'checked' : '' }}>
      @else
        <input type="radio" class="custom-control-input" id="GenderMF" name="gender" value="3" {{ old('gender', $product->gender) == "3" ? 'checked' : '' }}>
      @endif
      <label class="custom-control-label" for="GenderMF">M+K</label>
    </div>
  </div>
</div>
