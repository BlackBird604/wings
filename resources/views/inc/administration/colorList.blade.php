@foreach($colors as $color)
  <div class="p-1 col-lg-4">
    <div class="card p-1">
      <div class="row p-1 pl-3">
        <div class="col-4 color-preview" style="background-color:{{$color->value}};"></div>
        <div class="col-8">
          <h4 class="text-dark">{{$color->name}}</h4>
          <h4 class="text-dark">{{$color->value}}</h4>
          <div class="row justify-content-center">
            <div class="col-4">
              <a href="/colors/{{$color->id}}/edit" class="btn btn-outline-primary">Edytuj</a>
            </div>
            <div class="col-4">
              {!!Form::open(['action' => ['ColorController@destroy', $color->id], 'method' => 'POST'])!!}
              {{Form::hidden('_method', 'DELETE')}}
              {{Form::submit('Usuń', ['class' => 'btn btn-outline-danger'])}}
              {!!Form::close()!!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endforeach
