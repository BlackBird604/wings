@foreach($products as $product)
  <div class="p-1 col-lg-4">
    <div class="card p-1">
      <div class="card-body py-0">
      <div class="row p-0">
        <div class="col-6 px-0">
          <div class="w-100 image-box">
            <img src={{asset("storage/images/$product->image")}} class="h-100 mx-auto d-block">
          </div>
        </div>
        <div class="col-6 d-inline-block">
          <h3 class="ml-2 text-dark text-truncate">
            {{$product->name}}
          </h3>

          <h4>
            {{$product->price}} zł
          </h4>

          <div class="row w-100 ml-1 justify-content-center">
            <h5 class="mr-1">
                ({{$sizes->firstWhere('id',$product->size_id)->shortName}}) /
            </h5>
            @switch($product->gender)
              @case(1)
                <img src={{asset("storage/icons/male.png")}} style="width:20px; height:20px;">
              @break
              @case(2)
                <img src={{asset("storage/icons/female.png")}} style="width:20px; height:20px;">
              @break
              @case(3)
                <img src={{asset("storage/icons/male.png")}} style="width:20px; height:20px;">
                <img src={{asset("storage/icons/female.png")}} style="width:20px; height:20px;">
              @break
            @endswitch
          </div>

          <div class="row justify-content-around mr-2">
            <div class="col-4">
              <a href="/products/{{$product->id}}/edit" class="btn btn-outline-primary">Edytuj</a>
            </div>
            <div class="col-4">
              {!!Form::open(['action' => ['ProductController@destroy', $product->id], 'method' => 'POST'])!!}
              {{Form::hidden('_method', 'DELETE')}}
              {{Form::submit('Usuń', ['class' => 'btn btn-outline-danger'])}}
              {!!Form::close()!!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
@endforeach
