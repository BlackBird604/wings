@foreach($productTypes as $productType)
  <div class="p-1 col-md-3">
    <div class="card p-1">
      <div class="card-body">
        <h1>{{$productType->name}}</h1>
        <div class="row w-100 m-0 p-0 justify-content-center">
          <div class="col-6">
            <a href="/productTypes/{{$productType->id}}/edit" class="btn btn-outline-primary">Edytuj</a>
          </div>
          <div class="col-6">
            {!!Form::open(['action' => ['ProductTypeController@destroy', $productType->id], 'method' => 'POST'])!!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Usuń', ['class' => 'btn btn-outline-danger'])}}
            {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endforeach
