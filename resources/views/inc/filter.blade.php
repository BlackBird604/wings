<div class="container text-center">
  <div class="card mb-4 box-shadow bg-dark mt-2">
    <div class="card-body p-2 px-4">
      <form class="ml-auto" method="get" action="/search">
        <div class="form-row">

          <!-- Nazwa produktu -->
          <div class="input-group col-lg-4 p-1">
            <input class="form-control" type="text" name="name" placeholder="Nazwa produktu" value="{{Request()->name}}">
          </div>

          <!-- Rodzaj produktu -->
          <div class="input-group col-lg-4 p-1">
            <select name="productType" class="form-control browser-default custom-select">
              <option value>Rodzaj produktu</option>
              @foreach($productTypes as $productType)
                {
                  <option {{$productType->id == Request()->productType ? 'selected' : ''}}  value="{{ $productType->id }}">{{ $productType->name }}</option>
                }
              @endforeach
            </select>
          </div>

          <!-- Kolor i rozmiar produktu -->
          <div class="input-group col-lg-4 p-1">
            <select name="color" class="form-control browser-default custom-select">
              <option value>Kolor</option>
                @foreach($colors as $color)
                {
                  <option {{$color->id == Request()->color ? 'selected' : ''}}  value="{{ $color->id }}">{{ $color->name }}</option>
                }
                @endforeach
            </select>
          </div>
        </div>

        <div class="form-row">

          <div class="input-group col-lg-4 p-1">
            <div class="row w-100 p-0 m-0">
              <div class="col pl-0 pr-1">
                <input type="text" name="priceMin" class="form-control" placeholder="Cena Od" value="{{old('priceMin')}}">
              </div>
              <div class="col pl-1 pr-0">
                <input type="text" name="priceMax" class="form-control" placeholder="Cena Do" value="{{old('priceMax')}}">
              </div>
            </div>
          </div>

          <div class="input-group col-lg-4 p-1">
            <div class="row w-100 p-0 m-0">

              <div class="col-6 pl-0 pr-1">
                <select name="size" class="form-control browser-default custom-select">
                  <option value>Rozmiar</option>
                    @foreach($sizes as $size)
                    {
                      <option {{$size->id == Request()->size ? 'selected' : ''}}  value="{{ $size->id }}">{{ $size->shortName }}</option>
                    }
                    @endforeach
                </select>
              </div>

              <div class="col-3 p-0 m-0 d-flex align-items-center justify-content-center text-center">
                <div class="custom-control custom-checkbox custom-control-inline w-100 d-flex justify-content-center m-0">
                  <input {{Request()->genderM ? 'checked' : ''}}  type="checkbox" name="genderM" class="custom-control-input" id="genderM">
                  <label class="custom-control-label" for="genderM">
                    <img src={{asset("storage/icons/male.png")}} style="width:20px; height:20px;">
                  </label>
                </div>
              </div>

              <div class="col-3 pl-0 pr-0 d-flex align-items-center justify-content-center">
                <div class="custom-control custom-checkbox custom-control-inline w-100 d-flex justify-content-center m-0">
                  <input {{Request()->genderF ? 'checked' : ''}} type="checkbox" name="genderF" class="custom-control-input" id="genderF">
                  <label class="custom-control-label" for="genderF">
                    <img src={{asset("storage/icons/female.png")}} style="width:20px; height:20px;">
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div class="input-group col-lg-4 p-1">
            <button class="form-control btn btn-info" type="submit">Szukaj</button>
          </div>

        </div>
      </form>
    </div>
  </div>
</div>
