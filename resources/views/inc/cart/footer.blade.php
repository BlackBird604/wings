<div class="card-footer m-0 p-0">
  <div class="row m-0 p-0">
    <div class="col-1 d-flex align-items-center justify-content-center py-2 border-right">
    </div>
    <div class="col-5 d-flex align-items-center justify-content-center py-2 border-right">

    </div>
    <div class="col-1 d-flex align-items-center justify-content-center py-2 border-right">

    </div>
    <div class="col-1 d-flex align-items-center justify-content-center py-2 border-right">

    </div>
    <div class="col-1 d-flex align-items-center justify-content-center py-2 border-right">

    </div>
    <div class="col-2 d-flex align-items-center justify-content-center py-2 border-right">
      <h5 class="mb-0">{{number_format($cartTotal, 2, '.', '')}}</h5>
    </div>
    <div class="col-1 d-flex align-items-center justify-content-center py-2">
      <a href="/cart/clear" class="btn delete-btn m-0 ml-1 p-0">
        <img type="submit" class="clear-cart-icon" src={{asset("storage/icons/clearCart.png")}}>
      </a>
    </div>
  </div>
</div>
