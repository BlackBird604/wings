<div class="row m-0 p-0">
  <div class="col-3 ml-auto w-100 px-0 py-2">
    <a href="/cart/buy" class="btn btn-primary w-100 py-2 buy-btn">
      <h5 class="mb-0">
        <i>Przejdź do kasy</i>
      </h5>
    </a>
  </div>
</div>
