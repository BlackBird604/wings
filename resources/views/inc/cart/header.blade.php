<div class="card-header m-0 p-0">
  <div class="row m-0 p-0">
    <div class="col-1 d-flex justify-content-center py-2 border-right">
      <h5 class="mb-0">Zdjęcie</h5>
    </div>
    <div class="col-5 d-flex justify-content-center py-2 border-right">
      <h5 class="mb-0">Nazwa</h5>
    </div>
    <div class="col-1 d-flex justify-content-center py-2 border-right">
      <h5 class="mb-0">Rozmiar</h5>
    </div>
    <div class="col-1 d-flex justify-content-center py-2 border-right">
      <h5 class="mb-0">Ilość</h5>
    </div>
    <div class="col-1 d-flex justify-content-center py-2 border-right">
      <h5 class="mb-0">Cena</h5>
    </div>
    <div class="col-2 d-flex justify-content-center py-2 border-right">
      <h5 class="mb-0">Cena łączna</h5>
    </div>
    <div class="col-1 d-flex justify-content-center py-2">
      <h5 class="mb-0">Usuń</h5>
    </div>
  </div>
</div>
