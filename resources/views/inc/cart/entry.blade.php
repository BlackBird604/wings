<li class="list-group-item p-0 m-0">
  <div class="row m-0 p-0">
    <div class="col-1 d-flex justify-content-center py-1 px-1 border-right">
      <div class="w-100 image-box">
        <img src={{asset("storage/images/$product->image")}} class="h-100 mx-auto d-block">
      </div>
    </div>
    <div class="col-5 d-flex align-items-center justify-content-center border-right">
      <h5 class="mb-0"><b>{{$product->name}}</b></h5>
    </div>
    <div class="col-1 d-flex align-items-center justify-content-center border-right">
      <h6 class="mb-0">{{$size->shortName}}</h6>
    </div>
    <div class="col-1 d-flex align-items-center justify-content-center border-right">
      @if($cartEntry->amount > $product->amount)
        <img class="warning-icon" src={{asset("storage/icons/warning.png")}}>
      @endif
      <h6 class="mb-0">{{$adjustedAmount}}</h6>
    </div>
    <div class="col-1 d-flex align-items-center justify-content-center border-right">
      <h6 class="mb-0">{{$product->price}}</h6>
    </div>
    <div class="col-2 d-flex align-items-center justify-content-center border-right">
      <h6 class="mb-0">{{number_format($entryPrice, 2, '.', '')}}</h6>
    </div>
    <div class="col-1 d-flex align-items-center justify-content-center border-right">
      {!!Form::open(['action' => ['CartController@destroy', $cartEntry->id], 'method' => 'POST'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        <button type="submit" class="btn delete-btn">
          <img type="submit" class="delete-icon" src={{asset("storage/icons/deleteEntry.png")}}>
        </button>
      {!!Form::close()!!}
    </div>
  </div>
</li>
