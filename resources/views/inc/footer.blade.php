@section('styles')
  <link href="{{ asset('css/footer.css') }}" rel="stylesheet">
@endsection

<div id="footer" class="text-center">
  <br>Aplikacja webowa przeznaczona dla miłośników ptaków.<br>
  Projekt i wykonanie: Damian Werner; 3. rok Informatyki; Politechnika Opolska &copy; 2019.
  <br><br>
</div>
