<div class="col-md-3 text-light p-1">
  <a href="/products/{{$product->id}}">
    <div class="card text-center w-100 product-card ">
      <img class="card-img-top" src="storage/images/{{$product->image}}"></img>
      <div class="m-0 pt-2 justify-content-around bg-dark text-box">
        <h3 class="d-inline-block text-truncate m-0 p-0 text-light">
          {{$product->name}}
          <small>({{$sizes->firstWhere('id',$product->size_id)->shortName}})</small>
        </h3>
        <h5 class="text-warning m-0 pb-2">
          {{$product->price}}zł
        </h5>
      </div>
    </div>
  </a>
</div>
