<div class="row p-2 pb-4 color-box" data-toggle="buttons">
    @foreach($colors as $color)
      <div class="p-1" style="width:100px">
        @if(empty($product))
          <div class="card btn btn-info p-1 m-0 text-center w-100 {{ old('color') == $color->id ? 'active' : '' }}" style="height:120px; border-radius:25px;">
        @else
          <div class="card btn btn-info p-1 m-0 text-center w-100 {{ old('color', $product->color_id) == $color->id ? 'active' : '' }}" style="height:120px; border-radius:25px;">
        @endif
          <div class="card h-100 color-preview" style="background-color:{{$color->value}};">
            @if(empty($product))
              <input type="radio" name="color" value="{{$color->id}}" autocomplete="off"  {{ old('color') == $color->id ? 'checked' : '' }}>
            @else
              <input type="radio" name="color" value="{{$color->id}}" autocomplete="off"  {{ old('color', $product->color_id) == $color->id ? 'checked' : '' }}>
            @endif
          </div>
          <i class="text-dark">{{$color->name}}</i><br>
          <i class="text-dark">{{$color->value}}</i>
        </div>
      </div>
    @endforeach
</div>
