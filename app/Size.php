<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
  protected $table = 'sizes';
  public $primaryKey = 'id';
  public $timestamps = false;
}
