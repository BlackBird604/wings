<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartEntry extends Model
{
  protected $table = 'cart_entries';
  public $primaryKey = 'id';
  public $timestamps = false;
}
