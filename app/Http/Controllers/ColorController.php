<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;

use Redirect;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('colors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name'                  =>  'required',
          'value'                 =>  'required|regex:/#[a-zA-Z0-9]{6}$/'
        ]);

        $color = new Color;
        $color->name = $request->get('name');
        $color->value = $request->get('value');
        $color->save();
        return redirect()->action('AdministrationController@colorList')->with('success', 'Pomyślnie dodano kolor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $color = Color::find($id);
      return view('colors.edit')->with('color', $color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name'                  =>  'required',
          'value'                 =>  'required|regex:/#[a-zA-Z0-9]{6}$/'
        ]);

        $color = Color::find($id);
        $color->name = $request->get('name');
        $color->value = $request->get('value');
        $color->save();
        return redirect()->action('AdministrationController@colorList')->with('success', 'Pomyślnie edytowano kolor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $color = Color::find($id);

      $color->delete();
      return Redirect::back()->with('success', 'Pomyślnie usunięto kolor');
    }
}
