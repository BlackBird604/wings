<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use App\Size;
use App\Product;
use App\ProductType;

class AdministrationController extends Controller
{
  public function index()
  {
    return $this->productList();
  }

  public function productList()
  {
    $products = Product::all();
    $sizes = Size::all();
    return view('administration.index')->with('products', $products)->with('sizes', $sizes)->with('tabNumber', 0);
  }

  public function colorList()
  {
    $colors = Color::all();
    return view('administration.index')->with('colors', $colors)->with('tabNumber', 1);
  }

  public function sizeList()
  {
    $sizes = Size::all();
    return view('administration.index')->with('sizes', $sizes)->with('tabNumber', 2);
  }

  public function productTypeList()
  {
    $productTypes = ProductType::all();
    return view('administration.index')->with('productTypes', $productTypes)->with('tabNumber', 3);
  }

}
