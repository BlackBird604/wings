<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Size;

use Redirect;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'shortName'                 =>  'required'
        ]);

        $size = new Size;
        $size->shortName = $request->get('shortName');
        $size->save();
        return redirect()->action('AdministrationController@sizeList')->with('success', 'Pomyślnie dodano rozmiar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $size = Size::find($id);
        return view('sizes.edit')->with('size', $size);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'shortName'                 =>  'required'
        ]);

        $size = Size::find($id);
        $size->shortName = $request->get('shortName');
        $size->save();
        return redirect()->action('AdministrationController@sizeList')->with('success', 'Pomyślnie edytowano rozmiar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $size = Size::find($id);

      $size->delete();
      return Redirect::back()->with('success', 'Pomyślnie usunięto rozmiar');
    }
}
