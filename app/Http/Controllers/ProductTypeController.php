<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductType;
use Redirect;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name'                  =>  'required'
        ]);

        $type = new ProductType;
        $type->name = $request->get('name');
        $type->save();
        return redirect()->action('AdministrationController@productTypeList')->with('success', 'Pomyślnie dodano rodzaj produktu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productType = ProductType::find($id);
        return view('productTypes.edit')->with('productType', $productType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name'                  =>  'required'
        ]);

        $productType = ProductType::find($id);
        $productType->name = $request->get('name');
        $productType->save();
        return redirect()->action('AdministrationController@productTypeList')->with('success', 'Pomyślnie edytowano rodzaj produktu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $productType = ProductType::find($id);

      $productType->delete();
      return Redirect::back()->with('success', 'Pomyślnie usunięto rodzaj produktu');
    }
}
