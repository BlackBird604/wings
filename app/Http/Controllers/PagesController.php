<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Event;
use App\Category;

class PagesController extends Controller
{
    public function index()
    {
        return view('pages.home');
    }

    public function contact()
    {
        return view('pages.contact');
    }
}
