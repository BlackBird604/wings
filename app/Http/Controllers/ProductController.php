<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Redirect;
use App\Product;
use App\ProductType;
use App\Size;
use App\Color;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('amount', '>', 0)->orderBy('name', 'asc')->paginate(12);
        $productTypes = ProductType::orderBy('name', 'asc')->get();
        $colors = Color::orderBy('name', 'asc')->get();
        $sizes = Size::orderBy('shortName', 'asc')->get();
        return view('products.index')->with('products', $products)->with('productTypes', $productTypes)
                    ->with('colors', $colors)->with('sizes', $sizes);
    }

    public function search(Request $request)
    {
        $name_part = $request->get('name');
        $products = Product::where('amount', '>', 0);
        $products->where('name', 'like', '%'.$name_part.'%');

        if ($request->filled('productType')) {
          $productType = $request->get('productType');
          $products->where('productType_id', '=', $productType);
        }

        if ($request->filled('color')) {
          $color = $request->get('color');
          $products->where('color_id', '=', $color);
        }

        if ($request->filled('priceMin')) {
          $priceMin = $request->get('priceMin');
          $products->where('price', '>=', $priceMin);
        }

        if ($request->filled('priceMax')) {
          $priceMax = $request->get('priceMax');
          $products->where('price', '<=', $priceMax);
        }

        if ($request->filled('size')) {
          $size = $request->get('size');
          $products->where('size_id', '=', $size);
        }

        $gender = 0;
        $gender = $gender + ($request->filled('genderM') ? 1 : 0);
        $gender = $gender + ($request->filled('genderF') ? 2 : 0);
        if ($gender > 0)
        {
          $products->where('gender', '=', $gender);
        }


        $products = $products->orderBy('name', 'asc')->paginate(12);
        $productTypes = ProductType::orderBy('name', 'asc')->get();
        $colors = Color::orderBy('name', 'asc')->get();
        $sizes = Size::orderBy('shortName', 'asc')->get();
        return view('products.index')->with('products', $products)->with('productTypes', $productTypes)
                    ->with('colors', $colors)->with('sizes', $sizes);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productTypes = ProductType::all();
        $sizes = Size::all();
        $colors = Color::orderBy('name', 'asc')->get();
        return view('products.create')->with('productTypes', $productTypes)->with('sizes', $sizes)->with('colors', $colors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name'                  =>  'required',
        'productType'           =>  'required',
        'size'                  =>  'required',
        'gender'                =>  'required',
        'color'                 =>  'required',
        'amount'                =>  'required|integer|min:1',
        'price'                 =>  'required|numeric|min:0',
        'thumbnail'             =>  'nullable|mimes:jpeg,png'
      ]);

      $product = new Product;
      $product->name = $request->get('name');
      $product->productType_id = $request->get('productType');
      $product->size_id = $request->get('size');
      $product->gender = $request->get('gender');
      $product->color_id = $request->get('color');
      $product->amount = $request->get('amount');
      $product->price = $request->get('price');
      $product->description = $request->get('description');

      if ($request->hasFile('thumbnail'))
      {
          $image = $request->file('thumbnail');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          Image::make($image)->save(public_path('storage/images/' . $filename));
          $product->image = $filename;
      }

      $product->save();
      return redirect()->action('AdministrationController@productList')->with('success', 'Pomyślnie dodano produkt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $productType = ProductType::find($product->productType_id);
        $color = Color::find($product->color_id);
        $size = Size::find($product->size_id);
        return view('products.show')->with('product', $product)->with('productType', $productType)->with('color', $color)->with('size', $size);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $product = Product::find($id);
      $productTypes = ProductType::all();
      $sizes = Size::all();
      $colors = Color::orderBy('name', 'asc')->get();
      return view('products.edit')->with('product', $product)->with('productTypes', $productTypes)->with('sizes', $sizes)->with('colors', $colors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name'                  =>  'required',
          'productType'           =>  'required',
          'size'                  =>  'required',
          'gender'                =>  'required',
          'color'                 =>  'required',
          'amount'                =>  'required|integer|min:1',
          'price'                 =>  'required|numeric|min:0',
          'thumbnail'             =>  'nullable|mimes:jpeg,png'
        ]);

        $product = Product::find($id);
        $product->name = $request->get('name');
        $product->productType_id = $request->get('productType');
        $product->size_id = $request->get('size');
        $product->gender = $request->get('gender');
        $product->color_id = $request->get('color');
        $product->amount = $request->get('amount');
        $product->price = $request->get('price');
        $product->description = $request->get('description');

        if ($request->hasFile('thumbnail'))
        {
            $image = $request->file('thumbnail');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save(public_path('storage/images/' . $filename));
            $product->image = $filename;
        }

        $product->save();
        return redirect()->action('AdministrationController@productList')->with('success', 'Pomyślnie edytowano produkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $product = Product::find($id);

      $product->delete();
      return Redirect::back()->with('success', 'Pomyślnie usunięto produkt');
    }

    public static function getColorName($product)
    {
        $color = Color::find($product->color_id);
        return $color->name;
    }

    public static function getSizeName($product)
    {
        $size = Size::find($product->size_id);
        return $size->shortName;
    }

    public static function getTypeName($product)
    {
        $type = ProductType::find($product->productType_id);
        return $type->name;
    }
}
