<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use Storage;
use Carbon\Carbon;
use App\User;
use App\Size;
use App\Product;
use App\CartEntry;


class CartController extends Controller
{
    public function index()
    {
        $cartEntries = CartEntry::where('user_id', '=', Auth::id());
        $product_ids = $cartEntries->pluck('product_id')->toArray();
        $cartEntries = $cartEntries->get();
        $products = Product::whereIn('id', $product_ids)->get();
        $size_ids = $products->pluck('size_id')->toArray();
        $sizes = Size::whereIn('id', $size_ids)->get();
        return view('cart.index')->with('cartEntries', $cartEntries)->with('products', $products)->with('sizes', $sizes);
    }

    public function addProduct(Request $request, $product_id)
    {
        $amount = $request->get('amount');
        $user = Auth::user();
        $cartEntries = CartEntry::where('user_id', '=', $user->id);
        $cartEntries->where('product_id', '=', $product_id);

        if($cartEntries->count())
        {
          $cartEntry = $cartEntries->first();
          $cartEntry->amount = $cartEntry->amount + $amount;
          if($cartEntry->amount > Product::find($product_id)->amount)
          {
              return Redirect::back()->with('error', 'Niewystarczająca ilość towaru na stanie');
          }
          $cartEntry->save();
        }
        else
        {
          $cartEntry = new CartEntry;
          $cartEntry->user_id = $user->id;
          $cartEntry->product_id = $product_id;
          $cartEntry->amount = $amount;
          $cartEntry->save();
        }
        return Redirect::back()->with('success', 'Dodano produkt do koszyka');
    }

    public function destroy($id)
    {
        $cartEntry = CartEntry::find($id);

        $cartEntry->delete();
        return Redirect::back()->with('success', 'Pomyślnie usunięto wpis');
    }

    public function clear()
    {
        $user = Auth::user();
        $cartEntries = CartEntry::where('user_id', '=', $user->id)->delete();

        return Redirect::back()->with('success', 'Koszyk został wyczyszczony');
    }

    public function buy()
    {
        $user = Auth::user();
        $cartEntries = CartEntry::where('user_id', '=', $user->id);
        $this->saveTransactionLog($cartEntries->get());
        $this->reduceProductAmounts($cartEntries->get());
        $cartEntries->delete();
        return Redirect::back()->with('success', 'Zakup zakończony powodzeniem');
    }

    private function saveTransactionLog($cartEntries)
    {
        Storage::append('transactions.log', 'Transaction: ' . Carbon::now());
        Storage::append('transactions.log', 'UserID: ' . Auth::user()->id);
        $entryCounter = 1;
        $total = 0;
        foreach ($cartEntries as $cartEntry)
        {
            $product = Product::find($cartEntry->product_id);
            $adjustedAmount = $cartEntry->amount > $product->amount ? $product->amount : $cartEntry->amount;
            Storage::append('transactions.log', "\tEntry: " . $entryCounter);
            Storage::append('transactions.log', "\t\tProductID: " . $product->id);
            Storage::append('transactions.log', "\t\tProductName: " . $product->name);
            Storage::append('transactions.log', "\t\tType: " . ProductController::getTypeName($product));
            Storage::append('transactions.log', "\t\tColor: " . ProductController::getColorName($product));
            Storage::append('transactions.log', "\t\tSize: " . ProductController::getSizeName($product));
            Storage::append('transactions.log', "\t\tAmount: " . $adjustedAmount);
            $entryPrice = $product->price * $adjustedAmount;
            Storage::append('transactions.log', "\t\tEntryPrice: " . number_format($entryPrice, 2, '.', '')  . " zł");
            $total = $total + $entryPrice;
            $entryCounter = $entryCounter + 1;
        }
        Storage::append('transactions.log', "\tTotalPrice: " . number_format($total, 2, '.', '') . " zł");
    }

    private function reduceProductAmounts($cartEntries)
    {
        foreach ($cartEntries as $cartEntry)
        {
            $product = Product::find($cartEntry->product_id);
            $adjustedAmount = $cartEntry->amount > $product->amount ? $product->amount : $cartEntry->amount;
            $product->amount = $product->amount - $adjustedAmount;
            $product->save();
        }
    }
}
