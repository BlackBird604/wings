document.getElementById('thumbnail-input').addEventListener('change', readURL, true);
function readURL(){
    var file = document.getElementById("thumbnail-input").files[0];
    var reader = new FileReader();
    reader.onloadend = function(){
        document.getElementById('product-thumbnail').style.backgroundImage = "url(" + reader.result + ")";
    }
    if(file){
        reader.readAsDataURL(file);
    }
}

$(document).ready(function()
{
  document.getElementById('product-thumbnail').style.backgroundImage = "url(" + thumbnail_path + ")";
});
