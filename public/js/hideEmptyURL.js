jQuery(document).ready(function($){
  $('form[method="get"]').submit(function() {
    $(this).find(":input").filter(function(){ return !this.value; }).attr("disabled", "disabled");
    return true;
  });
});
